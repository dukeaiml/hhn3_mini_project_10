# Mini-project 10: Rust Serverless Transformer Endpoint

## Requirements
- Dockerize Hugging Face Rust transformer
- Deploy container to AWS Lambda
- Implement query endpoint

## Description

This project is a simple implementation of a Rust serverless transformer endpoint. It uses the Bloom model from the Hugging Face repository. The model is a BPE-based model that is trained on a large corpus of English text. The model is then used to generate text from a prompt.  Docker is used to package the model and deploy it to AWS Lambda. The model is then deployed to AWS Lambda. The endpoint is then used to query the model.

## Installation:
- Install Docker
- Install AWS CLI
- Install Cargo-lambda

## Set up:
- Download Bloom pretrained model from this [Hugging Face link](https://huggingface.co/rustformers/bloom-ggml/blob/main/bloom-560m-q4_0-ggjt.bin)
- Modify the Bloom model directory in `main.rs` according to your file structure


### Usage locally:
- Run `cargo lambda watch` to run the function locally
- Run `http://localhost:9000/?text="your_message" ` on your browser.


## Dockerize the model:
- Create a `Dockerfile` in the root directory
- Add a new user in `Identity and Access Management` (IAM) on AWS console with the following permissions:
    - AdmisnitratorAccess
- Create a new private repository of Elastic Containter Registry (ECR) (with the name of mini10 for example)
- Log in to Docker, authenticate Docker to AWS ECR:
    ```bash
    aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin YOUR_AWS_ACCOUNT_NUMBER.dkr.ecr.us-east-1.amazonaws.com

    ```
Replace `YOUR_AWS_ACCOUNT_NUMBER` with your AWS account number. 
- Build the Docker image:
    ```bash
    docker buildx build --progress=plain --platform linux/arm64 -t mini10 .
    ```
- Tag the image:
    ```bash
    docker tag mini10:latest YOUR_AWS_ACCOUNT_NUMBER.dkr.ecr.us-east-1.amazonaws.com/mini10:latest
    ```
- Push the Docker image to ECR:
    ```bash
    docker push YOUR_AWS_ACCOUNT_NUMBER.dkr.ecr.us-east-1.amazonaws.com/mini10:latest
    ```

## Deploy the model to AWS Lambda:
- Create a new Lambda function with option Container Image, set up suitable Architecture (arm64 in this project), choose the Container image from the ECR repo to be deployed.
![Screenshot](images/lambda_create.png)
- Configure Settings: on Configuration tab, adjust the memory and timeout settings according to the needs of your models.
![Screenshot](images/setting.png)
- Enable Function URL in Function URL tab, click Create function URL button to generate a new URL endpoint for the Lambda function, set Auth type to manage accessibility.
## Screenshots
![Screenshot](images/lambda.png)
![Screenshot](images/loca_demo.png)

![Screenshot](images/local_terminal.png)
Docker build:
![Screenshot](images/docker_build.png)
ECR:
![Screenshot](images/ecr.png)
Endpoint demo:
![Screenshot](images/demo.png)