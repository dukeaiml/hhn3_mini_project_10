
FROM ghcr.io/cargo-lambda/cargo-lambda:latest as builder

WORKDIR /usr/src/app

COPY . .

RUN cargo lambda build --release --arm64

FROM public.ecr.aws/lambda/provided:al2-arm64

WORKDIR /rust_serverless

COPY --from=builder /usr/src/app/target/ ./

COPY --from=builder /usr/src/app/models/bloom-560m-q4_0-ggjt.bin ./models/

RUN if [ -d /rust_serverless/lambda/rust_serverless/ ]; then echo "Directory exists"; else echo "Directory does not exist"; fi

RUN if [ -f /rust_serverless/lambda/rust_serverless/bootstrap ]; then echo "File exists"; else echo "File does not exist"; fi

ENTRYPOINT ["/rust_serverless/lambda/rust_serverless/bootstrap"]
