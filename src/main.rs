use lambda_http::{run, service_fn, tracing, Body, Error, Request, RequestExt, Response};
use std::convert::Infallible;
use std::io::Write;
use std::path::PathBuf;

fn handle_inference_response(response: llm::InferenceResponse, resp_content: &mut String) -> Result<llm::InferenceFeedback, Infallible> {
    match response {
        llm::InferenceResponse::PromptToken(token) | llm::InferenceResponse::InferredToken(token) => {
            print!("{token}");
            std::io::stdout().flush().unwrap();
            resp_content.push_str(&token);
            Ok(llm::InferenceFeedback::Continue)
        },
        _ => Ok(llm::InferenceFeedback::Continue),
    }
}

fn generator(prompt: String) -> Result<String, Box<dyn std::error::Error>> {
    // Choose the specific tokenizer and the architecture of the used model
    let tokenizer = llm::TokenizerSource::Embedded;
    let model_architecture = llm::ModelArchitecture::Bloom;
    // extract model, use model bloom-560m-q4_0-ggjt to output coherent text based on input text
    let model = PathBuf::from("models/bloom-560m-q4_0-ggjt.bin");

    // dynamically load a language model and set it with proper arguments
    let model = llm::load_dynamic(
        Some(model_architecture),
        &model,
        tokenizer,
        Default::default(),
        llm::load_progress_callback_stdout,
    )?;

    let inference_req = llm::InferenceRequest {
        prompt: (&prompt).into(),
        parameters: &llm::InferenceParameters::default(),
        play_back_previous_tokens: false,
        maximum_token_count: Some(10),
    };

    // Start a session for the loaded model, preparing it for inference.
    let mut model_session = model.start_session(Default::default());
    let mut resp_content = String::new();
    let mut rng_num = rand::thread_rng();  
    
    let inference = model_session.infer::<Infallible>(
        model.as_ref(),
        &mut rng_num,
        &inference_req,
        &mut Default::default(),
        |response| handle_inference_response(response, &mut resp_content),
    );

    // handle the inference result
    match inference {
        Ok(_) => Ok(resp_content),
        Err(e) => Err(Box::new(e)),
    }
}

async fn request_operator(req: Request) -> Result<Response<Body>, Error> {
    // set the request from users or the default input
    let request = req.query_string_parameters_ref().and_then(|params| params.first("text")).unwrap();   

    // print related messages for success or error
    let message = match generator(request.to_string()) {
        Ok(result) => result,
        Err(e) => format!("Inference error: {:?}", e),
    };
    // print out the generated response for visualzation
    println!("Response from model: {:?}", message);

    // build HTTP Response Body 
    let response = Response::builder().status(200).header("content-type", "text/html").body(Body::from(message)).map_err(Box::new)?;

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing::init_default_subscriber();
    run(service_fn(request_operator)).await
}